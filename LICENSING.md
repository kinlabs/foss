---
layout: null
sigs: LICENSING.md.asc
date: 2021-09-29
---
# About LICENSING

It is primordial when it comes to cryptography that the user
can audit the code and acquire trust in the system.
Therefore all code linked to security and authentication MUST
be opensource for anyone to review
(security by offuscation is not an option).


All code is separated in 2 categories :

 - "security" specific code and libraries :
      * Open Source [GPLv3][GPL3] when specified,
      * Public Domain [CC0][CC0] otherwiser.
 - the rest of the code is [MIT licensed](https://mit-license.org/) unless otherwise specified.


Agreed on {{page.date}} by

 - Doctor I·T: Michel Combes [{{site.data.sigs.mgcsig}}]

(GPG signatures: [on {{page.date}}](LICENSING.md.asc))

[GPL3]: gpl-3.0-standalone.html
[CC0]: https://duckduckgo.com/?q=CC0+license+!g
